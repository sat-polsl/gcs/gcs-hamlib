FROM debian:10.11-slim

RUN apt-get update && apt-get install -y \
    libhamlib2 \
    libhamlib-utils
